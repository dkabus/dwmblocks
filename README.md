# dwmblocks
Modular status bar for dwm written in C.

This is the build of [Desmond Kabus](https://gitlab.com/dkabus/dwmblocks).

## Modifying blocks
The statusbar is made from text output from commandline programs.

Blocks are added via stdin. The input format is defined as follows.

**First line**: number of blocks, tab, delimiter on the left, tab, delimiter between blocks, tab, delimiter after blocks:
```
7	'<'	'|'	'>'
```

**All other lines** define a new block, i.e.: update interval in seconds, tab, update signal, tab, command:
```
15  10  amixer get Master | grep -o "\[[0-9]\+%\]" | sed "s/[^0-9]*//g;1q"
```

As an example, `statusbar.sh` contains a shell script running dwmblock using this format.

## Signalling changes
For example, the audio module has the update signal 10 by default.
Thus, running `pkill -RTMIN+10 dwmblocks` will update it.
