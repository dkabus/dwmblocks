#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<signal.h>
#include<X11/Xlib.h>
#include<ctype.h>

typedef struct {
	char command[256];
	char status[64];
	unsigned int interval;
	unsigned int signal;
} Block;

int isempty(const char *s);
void getcmds(int time);
void getsigcmds(int signal);
void getstatus(char *str);
void replace(char *str, char old, char new);
void setroot();
void setupsignals();
void sighandler(int num);
void sighandler(int signum);
void statusloop();
void termhandler(int signum);

static Block* blocks;
static Display *dpy;
static Window root;
static char *statuscat;
static char delim[3][8];
static char statusstr[256];
static int screen;
static int statusContinue = 1;
static size_t length;

int isempty(const char *s)
{
	while (*s) {
		if (!isspace(*s))
			return 0;
		s++;
	}
	return 1;
}

void replace(char *str, char old, char new)
{
	int N = strlen(str);
	for(int i = 0; i < N; i++)
		if(str[i] == old)
			str[i] = new;
}

void getcmd(const Block *block, char *output)
{
	FILE *cmdf = popen(block->command,"r");
	if (!cmdf || !fscanf(cmdf, "%[^\n\r]", output) || feof(cmdf) || ferror(cmdf) || isempty(output))
	{ strcpy(output, ""); }
	pclose(cmdf);
}

void getcmds(int time)
{
	Block* current;
	for(int i = 0; i < length; i++)
	{
		current = blocks + i;
		if ((current->interval != 0 && time % current->interval == 0) || time == -1)
			getcmd(current,current->status);
	}
}

void getsigcmds(int signal)
{
	Block *current;
	for (int i = 0; i < length; i++)
	{
		current = blocks + i;
		if (current->signal == signal)
			getcmd(current,current->status);
	}
}

void setupsignals()
{
	for(int i = 0; i < length; i++)
	{
		if (blocks[i].signal > 0)
			signal(SIGRTMIN+blocks[i].signal, sighandler);
	}
}

void getstatus(char *str)
{
	strcpy(str, delim[0]);
	int j = strlen(delim[0]), initialised = 0;
	for(int i = 0; i < length; i++)
	{
		if (strlen(blocks[i].status))
		{
			if (initialised)
			{
				strcpy(str + j, delim[1]);
				j += strlen(delim[1]);
			}
			initialised = 1;

			strcpy(str + j, blocks[i].status);
			j += strlen(blocks[i].status);
		}
	}
	strcpy(str + j, delim[2]);
}

void setroot()
{
	Display *d = XOpenDisplay(NULL);
	if (d) {
		dpy = d;
	}
	screen = DefaultScreen(dpy);
	root = RootWindow(dpy, screen);
	getstatus(statusstr);
	XStoreName(dpy, root, statusstr);
	XCloseDisplay(dpy);
}

void statusloop()
{
	setupsignals();
	int i = 0;
	getcmds(-1);
	while(statusContinue)
	{
		getcmds(i);
		setroot();
		sleep(1.0);
		i++;
	}
}

void sighandler(int signum)
{
	getsigcmds(signum-SIGRTMIN);
	setroot();
}

void termhandler(int signum)
{
	statusContinue = 0;
	exit(0);
}

int main(int argc, char** argv)
{
    char* line = NULL;
    size_t len = 0, i = 0;
	Block* b;

	// read number of blocks and delimiter
	scanf("%d\t'%7[^']'\t'%7[^']'\t'%7[^']'\n",
			&length, delim[0], delim[1], delim[2]);

	// read interval, signal and command
	blocks = (Block*)malloc(length*sizeof(Block));
	while (getline(&line, &len, stdin) != -1) {
		b = blocks + i;
		sscanf(line, "%d\t%d\t%255[^\n]",
				&blocks[i].interval, &blocks[i].signal, b->command);
		i++;
	}

	signal(SIGTERM, termhandler);
	signal(SIGINT, termhandler);
	statusloop();
	free(blocks);
}
