#!/bin/sh
cat << EOF | ./dwmblocks
4	' [ '	' | '	' ] '
5	0	printf '🔋 '; cat /sys/class/power_supply/BAT?/capacity
15	10	printf '🔊 '; amixer get Master | grep -o '\[[0-9]\+%\]' | sed 's/[^0-9]*//g;1q'
15	9	printf '🔆 '; xbacklight | cut -d. -f1
15	0	date '+🕑 %F %H:%M'
EOF
